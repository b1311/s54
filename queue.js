let collection = [];

// Write the queue functions below.

// Output all the elements of the queue
function print() {
   	return(collection)
}


// Adds element to the rear of the queue
function enqueue(name) {
	collection.push(name);
		return (collection);
}

let name = ["John", "Jane", "Bob", "Cherry"]
	


// Removes element from the front of the queue
function dequeue() {
  collection.shift()
  	return (collection)
}

// Show element at the front
function front() {
	collection[0]
		return collection[0]
}

// Show total number of elements
function size() {
	let length = collection.length;
	return length
}


// Outputs a Boolean value describing whether queue is empty or not

function isEmpty() {
	if (collection.length > 0){
			return false
		} else {
			return true
		}
	}


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
